require("dotenv").config();

const User = require("../models/userschema");

const jwt = require("jsonwebtoken");

async function authenticateToken(req, res, next) {
  const authheader = req.headers["authorization"];
  const token = authheader && authheader.split(" ")[1];
  try
  {
    const user = await User.findOne({ token: token });
    if (user == undefined) {
      return res.send("Session expired.Please login again...");
    }
    if (token == null) {
      return res.send("Something went wrong...");
    }
    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, decodeduser) => {
      if (err) {
        if(err.message=="jwt expired")
        {
          return res.send("Token expired.Please login again...")
        }
        return res.send(err.message);
      }
      req.user = decodeduser;
      req.dbuser=user;
      next();
    });
  }
  catch(err)
  {
    res.send(err)
  }
}

module.exports = authenticateToken;
