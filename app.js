require("dotenv").config();
const express = require("express");

const mongoose = require("mongoose");

const cors = require("cors");

const userfun = require("./controllers/user");

const taskfun = require("./controllers/task");

const multer = require("multer");

const https = require("https");

const fs = require("fs");

const options = {
  key: fs.readFileSync("key.pem"),
  cert: fs.readFileSync("cert.pem"),
};

const authenticateToken = require("./middleware/authorization");

const app = express();

const path = require("path");

app.use(express.json());

app.use("/uploads", express.static(path.join(__dirname, "/uploads")));

app.use(cors());

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "uploads");
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + path.extname(file.originalname));
  },
});
const fileFilter = (req, file, cb) => {
  if (file.mimetype == "image/jpeg" || file.mimetype == "image/png") {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

const upload = multer({ storage: storage, fileFilter: fileFilter });

mongoose
  .connect("mongodb://localhost:27017/usersdb", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
  })
  .then(() => {
    console.log("Mydb connected successfully...");
  })
  .catch((err) => {
    console.log(err.message);
  });

app.get("/", (req, res) => {
  res.send("Home page...");
});

app.post("/signup", upload.single("image"), userfun.signup);

app.post("/login", userfun.login);

app.get("/listusers", authenticateToken, userfun.listusers);

app.get("/getuser/:username", authenticateToken, userfun.getuser);

app.patch("/updateuser", authenticateToken, userfun.updateuser);

app.post("/deleteuser", authenticateToken, userfun.deleteuser);

app.post("/logout", authenticateToken, userfun.logout);

//...........CRUD OPERATIONS ON TASK................

app.post("/createtask", authenticateToken, taskfun.createtask);

app.get("/listtasks", authenticateToken, taskfun.listtasks);

app.get("/gettask/:desc", authenticateToken, taskfun.gettask);

app.post("/updatetask", authenticateToken, taskfun.updatetask);

app.post("/deletetask", authenticateToken, taskfun.deletetask);

const port = process.env.PORT || 4000;

https.createServer(options, app).listen(port);
